import { TestBed } from '@angular/core/testing';

import { FetchEmployeeDataService } from './fetch-employee-data.service';

describe('FetchEmployeeDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FetchEmployeeDataService = TestBed.get(FetchEmployeeDataService);
    expect(service).toBeTruthy();
  });
});
