import { Component, OnInit } from '@angular/core';
import { FetchEmployeeDataService } from '../services/fetch-employee-data.service';
@Component({
  selector: 'app-newemployee',
  templateUrl: './newemployee.component.html',
  styleUrls: ['./newemployee.component.css']
})
export class NewemployeeComponent implements OnInit {
  constructor(private empService: FetchEmployeeDataService) {}
  employee: any;
  ngOnInit() {
    this.empService.getEmployees().subscribe(response => {
      console.log(response);
      this.employee = response;
    });
  }
  showUserDetails(employee: any) {
    console.log(employee);
    sessionStorage.setItem('employee', JSON.stringify(employee));
  }
}
